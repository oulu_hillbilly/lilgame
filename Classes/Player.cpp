#include "Player.h"

using namespace cocos2d;

Player::Player()
{
}

Player::~Player()
{
}

Player * Player::create(cocos2d::Vec2 position)
{
	auto player = new Player();

	if (player->initWithFile("player.png")) {
		player->autorelease();
		player->initOptions();
		player->addEvents();
		player->setPosition(position);
		return player;
	}
	CC_SAFE_DELETE(player);
	return nullptr;
}

void Player::initOptions()
{
	this->setName("Player");
}

void Player::update(float dt)
{
	move(dt);
}

void Player::addEvents()
{
	auto keyListener = EventListenerKeyboard::create();

	keyListener->onKeyPressed = [this](EventKeyboard::KeyCode keyCode, Event* event) {
		switch (keyCode) {
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			dirX = -1;
			dirY = 0;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			dirX = 1;
			dirY = 0;
			break;
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			dirX = 0;
			dirY = 1;
			break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			dirX = 0;
			dirY = -1;
			break;
		
		}
	};
	this->_eventDispatcher->addEventListenerWithFixedPriority(keyListener, 30);
}

void Player::stopMoving()
{
	dirX = dirY = 0;
}

void Player::updateSpeed()
{
	updateSpeed(1);
}

void Player::updateSpeed(float nSpeed)
{
	speed += nSpeed;
}

void Player::updateScore()
{
	updateScore(1);
}

void Player::updateScore(int score)
{
	m_score += score;
}

int Player::getScore()
{
	return m_score;
}

void Player::move(float dt)
{
	if (dirX!=0)
	{
		auto posX = getPositionX();
		posX += dirX*speed;
		setPositionX(posX);
	}
	else if (dirY!=0) 
	{
		auto posY = getPositionY();
		posY += dirY*speed;
		setPositionY(posY);
	}
}
