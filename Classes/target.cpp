#include "target.h"
using namespace cocos2d;

Target::Target()
{
}

Target::~Target()
{
}

Target * Target::create(cocos2d::Size size)
{
	auto target = new Target;
	if (target->initWithFile("target.png")) {
		target->autorelease();
		target->initOptions();
		target->setNewRandPos(size);
		return target;
	}
	CC_SAFE_DELETE(target);
	return nullptr;
}

cocos2d::Vec2 Target::newRandomPosition(cocos2d::Size max)
{
	auto x = random<float>(0, max.width);
	auto y = random<float>(0, max.height);
	return Vec2(x,y);
}

cocos2d::Vec2 Target::newRandomPosition(cocos2d::Rect bounds)
{
	auto x = random<float>(bounds.getMinX(), bounds.getMaxX());
	auto y = random<float>(bounds.getMinY(), bounds.getMaxY());
	return Vec2(x, y);
}

void Target::initOptions()
{
	this->setName("Target");
}

void Target::setNewRandPos(cocos2d::Size size)
{
	auto w = this->getContentSize().width * this->getScaleX()/2;
	auto h = this->getContentSize().height * this->getScaleY()/2;

	Rect bounds = { 0+w,0+h,size.width-w,size.height-h };
	auto position = this->newRandomPosition(bounds);
	this->setPosition(position);
}
