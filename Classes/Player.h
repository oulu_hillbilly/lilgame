#ifndef PLAYER_H
#define PLAYER_H
#include "cocos2d.h"
#include "2d\CCSprite.h"
class Player : public cocos2d::Sprite
{
public:
	Player();
	~Player();
	static Player* create(cocos2d::Vec2 position);
	void initOptions();
	void update(float dt);
	void addEvents();

	void stopMoving();
	void updateSpeed();
	void updateSpeed(float nSpeed);
	void updateScore();
	void updateScore(int score);

	int getScore();

private:
	float dirX = 0;
	float dirY = 0;
	float speed = 1;

	int m_score = 0;
	void move(float dt);
};
#endif // !PLAYER_H
