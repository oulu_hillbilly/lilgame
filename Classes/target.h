#ifndef _TARGET_H
#define _TARGET_H
#include "cocos2d.h"
#include "2d\CCSprite.h"
class Target : public cocos2d::Sprite
{
public:
	Target();
	~Target();
	static Target* create(cocos2d::Size size);
	void initOptions();
	void setNewRandPos(cocos2d::Size size);

private:
	cocos2d::Vec2 newRandomPosition(cocos2d::Size max);
	cocos2d::Vec2 newRandomPosition(cocos2d::Rect bounds);
};
#endif _TARGET_H