#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "2d\CCSprite.h"
#include "Player.h"
#include "target.h"

class LilGame : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();

    void update(float dt);
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(LilGame);

private:

	Player *m_player;
	Target *m_target;
	cocos2d::Label *m_scoreLabel;
	void registerInputListeners();

	cocos2d::Size m_visibleSize;

	bool checkWallColl(cocos2d::Sprite *obj);
	bool checkCollisionBetween(cocos2d::Sprite *obj1, cocos2d::Sprite *obj2);

	void updateScoreLabel();
};

#endif // __HELLOWORLD_SCENE_H__
