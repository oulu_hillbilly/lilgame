#include "lilgame.h"
#include "SimpleAudioEngine.h"
#include <iostream>

USING_NS_CC;

Scene* LilGame::createScene()
{
    return LilGame::create();
}

// on "init" you need to initialize your instance
bool LilGame::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    m_visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    m_scoreLabel = Label::createWithTTF("My LilGame", "fonts/Marker Felt.ttf", 24);
    
    // position the label on the center of the screen
    m_scoreLabel->setPosition(Vec2(origin.x + m_visibleSize.width/2,
                            origin.y + m_visibleSize.height - m_scoreLabel->getContentSize().height));

    // add the label as a child to this layer
    this->addChild(m_scoreLabel, 1);

	registerInputListeners();

	m_player = Player::create(Vec2(m_visibleSize.width/2+origin.x, m_visibleSize.height/2+origin.y));

	this->addChild(m_player, 0);

	m_target = Target::create(m_visibleSize);

	this->addChild(m_target, 0);

    this->scheduleUpdate();
	
	/*this->_scheduler->schedule([this](float dt) {
		m_target->setNewRandPos(visibleSize);
	}, this,1.0f,CC_REPEAT_FOREVER, 0.0f, false, "mycall");*/
	
    return true;
}

void LilGame::update(float dt) {
	
	m_player->update(dt);
	
	if (checkWallColl(m_player)) {
		log("wall collision");
		m_player->stopMoving();
	}
	else if (checkCollisionBetween(m_player, m_target)) {
		m_player->updateSpeed();
		m_player->updateScore();
		updateScoreLabel();
		m_target->setNewRandPos(m_visibleSize);
	}
	
}


void LilGame::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
    
}



void LilGame::registerInputListeners() {
    auto eventListener = EventListenerKeyboard::create();

    eventListener->onKeyPressed = [](EventKeyboard::KeyCode keyCode, Event* event)  {
        switch(keyCode) {
				case EventKeyboard::KeyCode::KEY_ESCAPE:
					Director::getInstance()->end();
				break;
        }
	};
	this->_eventDispatcher->addEventListenerWithFixedPriority(eventListener, 30);
}

bool LilGame::checkWallColl(cocos2d::Sprite *obj)
{
	float w = obj->getContentSize().width*obj->getScaleX() / 2;
	float h = obj->getContentSize().height*obj->getScaleY()/2;
	// Check horizontal hit
	if (obj->getPositionX()+w > m_visibleSize.width || obj->getPositionX()-w<0)
		return true;
	
	// Check vertical hit
	if (obj->getPositionY() + h > m_visibleSize.height || obj->getPositionY() - h < 0)
		return true;

	return false;
}

bool LilGame::checkCollisionBetween(cocos2d::Sprite * obj1, cocos2d::Sprite *obj2)
{
	auto rect1 = obj1->getBoundingBox();
	auto rect2 = obj2->getBoundingBox();

	if (rect1.intersectsRect(rect2)) return true;

	return false;
}

void LilGame::updateScoreLabel()
{
	auto score = m_player->getScore();
	m_scoreLabel->setString("Score: " + std::to_string(score));
}
